#!/bin/sh
# Amogu.sh - the sussiest script ever
# Indexs your meme folder and copies the image of choice to your clipboard.
# Created by Mattheus Roxas under https://eupl.eu/.

# Preview stuff - thank you euro20179
export FIFO="/tmp/image-preview.fifo"

cache=$XDG_CACHE_HOME/amogu.sh
mkdir -p "$cache"

start_ueberzug () {
    rm -f "$FIFO"
	mkfifo "$FIFO"
	ueberzug layer --parser json < "$FIFO" 2>~/.cache/amogu.sh/error &
	exec 3> "$FIFO"
}
stop_ueberzug (){
    exec 3>&-
    rm -f "$FIFO"
}

preview_img () {
    [ -d "$1" ] && echo "$1 is a directory" || \
    printf '%s\n' '{"action": "add", "identifier": "image-preview", "path": "'"$1"'", "x": "2", "y": "1", "width": "'"$FZF_PREVIEW_COLUMNS"'", "height": "'"$FZF_PREVIEW_LINES"'"}' > "$FIFO"
}
[ "$1" = "preview_img" ] && { preview_img "$2"; exit; }

start_ueberzug

vent () {
	find "$path"/* | tac | fzf --color=16 --preview-window="left:50%:wrap" --preview "sh $0 preview_img {}" || stop_ueberzug
	stop_ueberzug
}

# CLI
USAGE="Usage: ${0##*/} [-hcdp]
	-d		Creates a Dragon window using your selection.
	-c		Copies your selection to the clipboard.
	-p		Specify the path to look in."

fail() { echo ${0##*/}: 1>&2 "$*"; exit 1; }

# Thanks iceblink for all the $path stuff.
path="~/media/memes"

while getopts hdcp: opt; do
	case "$opt" in
		c) SUSSYNESS=xclip ;;
		d) SUSSYNESS=dragon ;;
		p) path=$OPTARG ;;
		[h?]) fail "$USAGE" ;;
	esac
done

case "$SUSSYNESS" in
	xclip)
		file_name="$(vent)"
		mime_type="$(file -b --mime-type $file_name)"
		xclip -selection clipboard -t "$mime_type" "$file_name" || fail "Clipboard failed" ;;

	dragon) 
		file_name="$(vent)"
		dragon-drag-and-drop -x "$file_name" || fail "Dragon failed" ;;
esac

