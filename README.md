# Amogu.sh
> The sussiest script ever

![Sexy preview](./preview.png)

Indexes your meme folder and displays the files with a preview. Your selection will be then made ready for posting.

## Dependencies 
* [Überzug](https://github.com/seebye/ueberzug)
* [fzf](https://github.com/junegunn/fzf)
* [dragon](https://github.com/mwh/dragon)
* [xclip](https://github.com/astrand/xclip)

## Installation
Arch users can install `amogu.sh-git` via the AUR.

### From source
```
git clone https://codeberg.org/thepogsupreme/amogu.sh
cd amogu.sh
cp ./amogus.sh ~/.local/bin && chmod +x ~/.local/bin/amogu.sh
```

## Usage
```
amogu.sh: Usage: amogu.sh [-hcd]
	-d		Create a Dragon window using your selection.
	-c		Copy your selection to your clipboard.
```

## Kontribooting
![kontriboot](./kontriboot.png)
